package com.simple.ioc;

import com.simple.ioc.beans.Bean2;
import com.simple.ioc.container.BeanContainer;
import com.simple.ioc.container.BeanContainerUtils;

import java.util.Map;

public class StandaloneApplication {
    public static void main(String[] args) {
        IocRunner.run(StandaloneApplication.class);
        Bean2 bean2 = (Bean2) BeanContainerUtils.getInstance().getBean(Bean2.class);
        bean2.getReferences();
        for (Map.Entry mapentry : BeanContainer.getInstance().getContainer().entrySet())
            System.out.println("key = " + mapentry.getKey().toString() + " ====> reference = " + mapentry.getValue());
    }
}