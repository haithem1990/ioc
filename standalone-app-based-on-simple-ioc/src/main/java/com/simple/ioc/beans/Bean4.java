package com.simple.ioc.beans;

import com.simple.ioc.annotations.IocScan;

import javax.annotation.PostConstruct;

@IocScan
public class Bean4 {

    public Bean4() {
    }

    @PostConstruct
    public void init() {
        System.out.println("from @PostConstruct bean4 = hello every body");
    }
}
