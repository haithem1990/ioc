package com.simple.ioc.beans;

import com.simple.ioc.annotations.IocInject;
import com.simple.ioc.annotations.IocScan;

import javax.annotation.PostConstruct;

@IocScan
public class Bean3 {

    @IocInject
    public Bean2 bean2;

    public Bean3() {
    }

    public Bean2 getBean2() {
        return bean2;
    }

    @PostConstruct
    public void init(){
        System.out.println("from @PostConstruct bean3 = hello every body");
    }

    public void setBean2(Bean2 bean2) {
        this.bean2 = bean2;
    }
}
