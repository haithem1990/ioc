package com.simple.ioc.beans;

import com.simple.ioc.annotations.ExternalProperty;
import com.simple.ioc.annotations.IocScan;

import javax.annotation.PostConstruct;

@IocScan
public class Bean1 {
    @ExternalProperty(value = "ss")
    public String param;

    public Bean1() {
    }

    @PostConstruct
    public void init(){
        System.out.println("from @PostConstruct bean1 = hello every body");
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
}
