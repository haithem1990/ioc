package com.simple.ioc.beans;

import com.simple.ioc.annotations.IocScan;

import javax.annotation.PostConstruct;

@IocScan
public class Bean5 {

    public Bean5() {
    }

    @PostConstruct
    public void init() {
        System.out.println("from @PostConstruct bean5 = hello every body");
    }
}
