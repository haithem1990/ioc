package com.simple.ioc.beans;

import com.simple.ioc.annotations.IocInject;
import com.simple.ioc.annotations.IocScan;

import javax.annotation.PostConstruct;

@IocScan
public class Bean2 {

    @IocInject
    public Bean1 bean1;

    @IocInject
    public Bean4 bean4;

    public Bean2() {
    }

    @PostConstruct
    public void init() {
        System.out.println("from @PostConstruct bean2 = hello every body");
    }

    public void getReferences(){
        System.out.println("reference from bean2 : \n bean 1 = "+bean1);
        System.out.println("reference from bean2 : \n bean 4 = "+bean4);
    }

    public Bean1 getBean1() {
        return bean1;
    }

    public void setBean1(Bean1 bean1) {
        this.bean1 = bean1;
    }

    public Bean4 getBean4() {
        return bean4;
    }

    public void setBean4(Bean4 bean4) {
        this.bean4 = bean4;
    }
}
