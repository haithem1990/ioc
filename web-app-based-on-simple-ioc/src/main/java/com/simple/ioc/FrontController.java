package com.simple.ioc;

import com.simple.ioc.container.BeanContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class FrontController extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(IocRunner.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<meta charset=\"utf-8\" />");
        out.println("<title>Test</title>");
        out.println("</head>");
        out.println("<body>");
        for (Map.Entry mapentry : BeanContainer.getInstance().getContainer().entrySet()){
            out.println("<p> name = " + mapentry.getKey().toString() + " ====> reference = " + mapentry.getValue() + "</p>");
        }
        out.println("</body>");
        out.println("</html>");
    }
}
