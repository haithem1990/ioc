package com.simple.ioc;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class IocServletContextListener implements ServletContextListener {

    public static void run(Class<?> classRunner) {
        IocRunner.run(classRunner);
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        run(IocRunner.class);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
