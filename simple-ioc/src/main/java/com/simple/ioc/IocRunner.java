package com.simple.ioc;

import com.simple.ioc.annotations.IocScan;
import com.simple.ioc.container.BeanRegistry;
import com.simple.ioc.container.definitions.IocScanBeanDefinition;
import com.simple.ioc.cycle.BeanInitializer;
import com.simple.ioc.cycle.post.provider.PostProcessProviderTemplate;
import com.simple.ioc.properties.provider.PropertiesResolverProviderTemplate;
import com.simple.ioc.registry.provider.BeanRegistryProviderTemplate;
import com.simple.ioc.scanners.provider.ScannerProviderTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

public class IocRunner {

    private static final Logger logger = LoggerFactory.getLogger(IocRunner.class);
    public static final String COM_SIMPLE = "com.simple";

    public IocRunner() {
    }

    public static void run(Class<?> classRunner) {
        logger.info(" start run");
        PropertiesResolverProviderTemplate.resolveProperties();

        List<Class<?>> classList = Arrays.asList(getClasses(classRunner.getPackage().getName()));

        List<Class<?>> classListToScan = classList.stream().filter(
                item -> Arrays.asList(item.getDeclaredAnnotations()).stream().filter(annotation -> annotation.annotationType() == IocScan.class).findFirst().isPresent()
        ).collect(Collectors.toList());

        classListToScan.forEach(item -> {
            try {
                Class<?> classItem = Class.forName(item.getName());
                IocScanBeanDefinition iocScanBeanDefinition = new IocScanBeanDefinition();
                Annotation[] annotations = classItem.getAnnotations();
                iocScanBeanDefinition.setName(classItem.getName());
                iocScanBeanDefinition.setAnnotationList(Arrays.asList(annotations));
                iocScanBeanDefinition.setaClass(classItem);

                iocScanBeanDefinition = ScannerProviderTemplate.doScans(iocScanBeanDefinition);

                BeanRegistry.getInstance().getRegistry().put(classItem.getName(), iocScanBeanDefinition);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        });

        BeanRegistryProviderTemplate.processRegistry();

        BeanInitializer.initialize();

        PostProcessProviderTemplate.postProcess();
    }

    private static Class[] getClasses(String packageName) {
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            String path = packageName.replace('.', '/');
            Enumeration resources = classLoader.getResources(path);
            List dirs = new ArrayList();
            while (resources.hasMoreElements()) {
                URL resource = (URL) resources.nextElement();
                dirs.add(new File(resource.getFile()));
            }
            ArrayList classes = new ArrayList();
            for (Object directory : dirs) {
                classes.addAll(findClasses((File) directory, packageName));
            }
            return (Class[]) classes.toArray(new Class[classes.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List findClasses(File directory, String packageName) throws ClassNotFoundException {
        List classes = new ArrayList();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }
}
