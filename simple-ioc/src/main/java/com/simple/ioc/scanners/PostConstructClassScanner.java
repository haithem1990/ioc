package com.simple.ioc.scanners;

import com.simple.ioc.container.definitions.IocScanBeanDefinition;
import com.simple.ioc.container.definitions.MethodBeanDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PostConstructClassScanner implements ClassScanner {
    private static final Logger logger = LoggerFactory.getLogger(PostConstructClassScanner.class);

    @Override
    public IocScanBeanDefinition doScan(IocScanBeanDefinition iocScanBeanDefinition) {
        logger.info("start doScan for {}", iocScanBeanDefinition.getName());

        List<Method> methods = new ArrayList<>();
        List<Method> methodList = Arrays.asList(iocScanBeanDefinition.getaClass().getDeclaredMethods());
        if (methodList != null && !methodList.isEmpty()) {
            methodList.stream().forEach(item -> {
                List<Annotation> annotationList = Arrays.asList(item.getDeclaredAnnotations());
                if (annotationList != null && !annotationList.isEmpty() && annotationList.stream().filter(annotation -> annotation.annotationType() == PostConstruct.class).findFirst().isPresent()) {
                    methods.add(item);
                }
            });
        }
        try {
            for (Method method : methods) {
                MethodBeanDefinition methodBeanDefinition = new MethodBeanDefinition();
                methodBeanDefinition.setName(method.getName());
                methodBeanDefinition.getAnnotationList().add(method.getAnnotation(PostConstruct.class));
                iocScanBeanDefinition.getMethodBeanDefinitions().add(methodBeanDefinition);
            }
        } catch (Exception e) {
        }
        logger.info("end doScan for {}", iocScanBeanDefinition.getName());
        return iocScanBeanDefinition;
    }
}
