package com.simple.ioc.scanners;

import com.simple.ioc.annotations.IocInject;
import com.simple.ioc.container.definitions.IocInjectBeanDefinition;
import com.simple.ioc.container.definitions.IocScanBeanDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IocInjectClassScanner implements ClassScanner {
    private static final Logger logger = LoggerFactory.getLogger(IocInjectClassScanner.class);

    @Override
    public IocScanBeanDefinition doScan(IocScanBeanDefinition iocScanBeanDefinition) {
        logger.info("start doScan for {}", iocScanBeanDefinition.getName());

        try {
            List<Field> fields = new ArrayList<>();
            List<Field> fieldList = Arrays.asList(iocScanBeanDefinition.getaClass().getDeclaredFields());
            if (fieldList != null && !fieldList.isEmpty()) {
                fieldList.stream().forEach(item -> {
                    List<Annotation> annotationList = Arrays.asList(item.getDeclaredAnnotations());
                    if (annotationList != null && !annotationList.isEmpty() && annotationList.stream().filter(annotation -> annotation.annotationType() == IocInject.class).findFirst().isPresent()) {
                        fields.add(item);
                    }
                });
            }
            for (Field field : fields) {
                IocInjectBeanDefinition iocInjectBeanDefinition = new IocInjectBeanDefinition();
                iocInjectBeanDefinition.setName(field.getType().getName());
                iocInjectBeanDefinition.getAnnotationList().add(field.getAnnotation(IocInject.class));
                iocScanBeanDefinition.getIocInjectBeanDefinitions().add(iocInjectBeanDefinition);
            }
        logger.info("end doScan for {}", iocScanBeanDefinition.getName());
        return iocScanBeanDefinition;
        } catch (Exception e) {
            logger.error("error occured when {}", iocScanBeanDefinition.getName());
            e.printStackTrace();
            throw e;
        }
    }
}
