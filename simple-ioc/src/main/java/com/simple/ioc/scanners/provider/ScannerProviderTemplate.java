package com.simple.ioc.scanners.provider;

import com.simple.ioc.container.definitions.IocScanBeanDefinition;
import com.simple.ioc.scanners.ClassScanner;
import com.simple.ioc.scanners.IocInjectClassScanner;
import com.simple.ioc.scanners.ExternalPropertyClassScanner;
import com.simple.ioc.scanners.PostConstructClassScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class ScannerProviderTemplate {

    private static final Logger logger = LoggerFactory.getLogger(ScannerProviderTemplate.class);

    public static IocScanBeanDefinition doScans(IocScanBeanDefinition iocScanBeanDefinition) {
        logger.info("start doScans for {}",iocScanBeanDefinition.getName());
        List<ClassScanner> classScannerList = Arrays.asList(new IocInjectClassScanner(), new ExternalPropertyClassScanner(),new PostConstructClassScanner());
        for (ClassScanner classScanner : classScannerList) {
            iocScanBeanDefinition = doScan(classScanner, iocScanBeanDefinition);
        }
        logger.info("end doScans for {}",iocScanBeanDefinition.getName());
        return iocScanBeanDefinition;
    }

    private static IocScanBeanDefinition doScan(ClassScanner classScanner, IocScanBeanDefinition iocScanBeanDefinition) {
        return classScanner.doScan(iocScanBeanDefinition);
    }
}
