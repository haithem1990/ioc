package com.simple.ioc.scanners;

import com.simple.ioc.annotations.ExternalProperty;
import com.simple.ioc.container.definitions.IocInjectBeanDefinition;
import com.simple.ioc.container.definitions.IocScanBeanDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExternalPropertyClassScanner implements ClassScanner {
    private static final Logger logger = LoggerFactory.getLogger(ExternalPropertyClassScanner.class);

    @Override
    public IocScanBeanDefinition doScan(IocScanBeanDefinition iocScanBeanDefinition) {
        logger.info("start doScan for {}", iocScanBeanDefinition.getName());
        // get fields annotated with @ExternalProperty
        List<Field> fields = new ArrayList<>();
        List<Field> fieldList = Arrays.asList(iocScanBeanDefinition.getaClass().getDeclaredFields());
        if (fieldList != null && !fieldList.isEmpty()) {
            fieldList.stream().forEach(item -> {
                List<Annotation> annotationList = Arrays.asList(item.getDeclaredAnnotations());
                if (annotationList != null && !annotationList.isEmpty() && annotationList.stream().filter(annotation -> annotation.annotationType() == ExternalProperty.class).findFirst().isPresent()) {
                    fields.add(item);
                }
            });
        }

        try {
            for (Field field : fields) {
                IocInjectBeanDefinition iocInjectBeanDefinition = new IocInjectBeanDefinition();
                iocInjectBeanDefinition.setName(field.getName());
                iocInjectBeanDefinition.getAnnotationList().add(field.getAnnotation(ExternalProperty.class));
                iocScanBeanDefinition.getIocInjectBeanDefinitions().add(iocInjectBeanDefinition);
            }
        } catch (Exception e) {
        }
        logger.info("end doScan for {}", iocScanBeanDefinition.getName());
        return iocScanBeanDefinition;
    }
}
