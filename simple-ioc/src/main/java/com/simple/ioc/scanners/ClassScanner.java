package com.simple.ioc.scanners;

import com.simple.ioc.container.definitions.IocScanBeanDefinition;

public interface ClassScanner {

    IocScanBeanDefinition doScan(IocScanBeanDefinition iocScanBeanDefinition);
}
