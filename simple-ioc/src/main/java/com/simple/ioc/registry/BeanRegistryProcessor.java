package com.simple.ioc.registry;

public interface BeanRegistryProcessor {

    void process();
}
