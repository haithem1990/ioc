package com.simple.ioc.registry.provider;

import com.simple.ioc.registry.BeanRegistryProcessor;
import com.simple.ioc.registry.SorterBeanRegistryProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class BeanRegistryProviderTemplate {
    private static final Logger logger = LoggerFactory.getLogger(BeanRegistryProviderTemplate.class);

    public static void processRegistry() {
        logger.info("start processRegistry");
        List<BeanRegistryProcessor> beanRegistryProcessorList = Arrays.asList(new SorterBeanRegistryProcessor());
        for (BeanRegistryProcessor beanRegistryProcessor : beanRegistryProcessorList) {
            beanRegistryProcessor.process();
        }
        logger.info("end processRegistry");
    }
}
