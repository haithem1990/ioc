package com.simple.ioc.registry;

import com.simple.ioc.annotations.IocScan;
import com.simple.ioc.container.BeanRegistry;
import com.simple.ioc.container.definitions.BeanDefinition;
import com.simple.ioc.container.definitions.IocScanBeanDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SorterBeanRegistryProcessor implements BeanRegistryProcessor {
    private static final Logger logger = LoggerFactory.getLogger(SorterBeanRegistryProcessor.class);

    private List<String> orderedBeanDefinition = new ArrayList<>();

    public void process() {
        logger.info("start process");
        for (Map.Entry mapentry : BeanRegistry.getInstance().getRegistry().entrySet()) {
            sort(mapentry.getKey().toString());
        }
        sortBeanRegistry();
        logger.info("end process");
    }

    public void sortBeanRegistry() {
        LinkedHashMap<String, IocScanBeanDefinition> sortedList = new LinkedHashMap<>();
        for (String beanName : orderedBeanDefinition) {
            sortedList.put(beanName, BeanRegistry.getInstance().getRegistry().get(beanName));
        }
        BeanRegistry.getInstance().setRegistry(sortedList);
    }

    public void sort(String name) {
        if (!orderedBeanDefinition.contains(name) && BeanRegistry.getInstance().getRegistry().get(name) != null && BeanRegistry.getInstance().getRegistry().get(name).getAnnotationList().stream().filter(itm -> itm.annotationType() == IocScan.class).findFirst().isPresent()) {
            if (BeanRegistry.getInstance().getRegistry().get(name).getIocInjectBeanDefinitions().isEmpty()) {
                orderedBeanDefinition.add(name);
            } else {
                for (BeanDefinition cond : BeanRegistry.getInstance().getRegistry().get(name).getIocInjectBeanDefinitions()) {
                    sort(cond.getName());
                }
                orderedBeanDefinition.add(name);
            }
        }
    }
}
