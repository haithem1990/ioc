package com.simple.ioc.cycle.post.processors.set;

import com.simple.ioc.annotations.IocInject;
import com.simple.ioc.container.BeanContainer;
import com.simple.ioc.container.definitions.IocInjectBeanDefinition;
import com.simple.ioc.container.definitions.IocScanBeanDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

public class IocInjectPostProcess implements PostProcessSet {
    private static final Logger logger = LoggerFactory.getLogger(IocInjectPostProcess.class);

    @Override
    public void postProcess(String name) {
        logger.info("start postProcess for {}", name);
        IocScanBeanDefinition iocScanBeanDefinition = BeanContainer.getInstance().getBeanRegistry().getRegistry().get(name);

        if (iocScanBeanDefinition.getIocInjectBeanDefinitions() != null && !iocScanBeanDefinition.getIocInjectBeanDefinitions().isEmpty()) {
            List<IocInjectBeanDefinition> iocInjectBeanDefinitions = iocScanBeanDefinition.getIocInjectBeanDefinitions().stream().filter(item ->
                    item.getAnnotationList() != null && !item.getAnnotationList().isEmpty() && item.getAnnotationList().stream().filter(o -> o.annotationType() == IocInject.class).findFirst().isPresent()
            ).collect(Collectors.toList());

            resolve(name, iocInjectBeanDefinitions);
        }
        logger.info("end postProcess for {}", name);
    }


    private void resolve(String name, List<IocInjectBeanDefinition> iocInjectBeanDefinitions) {
        if (iocInjectBeanDefinitions != null && !iocInjectBeanDefinitions.isEmpty()) {
            for (IocInjectBeanDefinition iocInjectBeanDefinition : iocInjectBeanDefinitions) {
                resolve(name, iocInjectBeanDefinition);
            }
        }
    }

    private void resolve(String name, IocInjectBeanDefinition iocInjectBeanDefinition) {
        try {
            Object object = BeanContainer.getInstance().getContainer().get(name);
            String fieldName = iocInjectBeanDefinition.getName().substring(iocInjectBeanDefinition.getName().lastIndexOf(".") + 1);
            String cFieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            String methodName = "set" + cFieldName;
            Object fieldObject = BeanContainer.getInstance().getContainer().get(iocInjectBeanDefinition.getName());
            object.getClass().getMethod(methodName, fieldObject.getClass()).invoke(object, fieldObject);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
