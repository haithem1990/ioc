package com.simple.ioc.cycle.post.provider;

import com.simple.ioc.cycle.post.processors.PostProcess;
import com.simple.ioc.container.BeanContainer;
import com.simple.ioc.cycle.post.processors.after.PostConstructPostProcess;
import com.simple.ioc.cycle.post.processors.after.PostProcessAfterSet;
import com.simple.ioc.cycle.post.processors.set.ExternalPropertiesPostProcess;
import com.simple.ioc.cycle.post.processors.set.IocInjectPostProcess;
import com.simple.ioc.cycle.post.processors.set.PostProcessSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PostProcessProviderTemplate {
    private static final Logger logger = LoggerFactory.getLogger(PostProcessProviderTemplate.class);

    public static void postProcess() {
        logger.info("*******************************************");
        logger.info("start postProcess");
        List<PostProcessSet> postProcessSetList = Arrays.asList(new ExternalPropertiesPostProcess(), new IocInjectPostProcess());
        List<PostProcessAfterSet> postProcessAfterSetList = Arrays.asList(new PostConstructPostProcess());
        for (Map.Entry mapentry : BeanContainer.getInstance().getContainer().entrySet()) {
            // resolve dependecies of the condidate
            for (PostProcess postProcess : postProcessSetList) {
                postProcess.postProcess(mapentry.getKey().toString());
            }
            // execute methods after resolve dependecies (@PostContruct)
            for (PostProcessAfterSet postProcessAfterSet : postProcessAfterSetList) {
                postProcessAfterSet.postProcess(mapentry.getKey().toString());
            }
        }
        logger.info("end postProcess");
        logger.info("*******************************************");
    }
}
