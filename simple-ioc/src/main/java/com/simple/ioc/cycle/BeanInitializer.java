package com.simple.ioc.cycle;

import com.simple.ioc.container.BeanContainer;
import com.simple.ioc.container.BeanRegistry;
import com.simple.ioc.container.definitions.IocScanBeanDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class BeanInitializer {
    private static final Logger logger = LoggerFactory.getLogger(BeanInitializer.class);

    public static void initialize() {
        logger.info("##########################################");
        logger.info("start initialize beans");
        for (Map.Entry mapentry : BeanRegistry.getInstance().getRegistry().entrySet()) {

            Constructor<?> constructor = ((IocScanBeanDefinition) mapentry.getValue()).getaClass().getConstructors()[0];
            try {
                Object instance = constructor.newInstance();
                BeanContainer.getInstance().getContainer().put(mapentry.getKey().toString(), instance);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        logger.info("end initialize beans");
        logger.info("##########################################");

    }
}
