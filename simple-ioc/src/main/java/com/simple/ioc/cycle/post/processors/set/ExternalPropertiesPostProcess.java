package com.simple.ioc.cycle.post.processors.set;

import com.simple.ioc.annotations.ExternalProperty;
import com.simple.ioc.container.BeanContainer;
import com.simple.ioc.container.PropertiesRegistry;
import com.simple.ioc.container.definitions.IocInjectBeanDefinition;
import com.simple.ioc.container.definitions.IocScanBeanDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

public class ExternalPropertiesPostProcess implements PostProcessSet {
    private static final Logger logger = LoggerFactory.getLogger(ExternalPropertiesPostProcess.class);
    public static final String SET = "set";
    public static final String VALUE = "value";

    @Override
    public void postProcess(String name) {
        logger.info("start postProcess for {}", name);
        IocScanBeanDefinition iocScanBeanDefinition = BeanContainer.getInstance().getBeanRegistry().getRegistry().get(name);
        if (iocScanBeanDefinition.getIocInjectBeanDefinitions() != null && !iocScanBeanDefinition.getIocInjectBeanDefinitions().isEmpty()) {
            List<IocInjectBeanDefinition> iocInjectBeanDefinitions = iocScanBeanDefinition.getIocInjectBeanDefinitions().stream().filter(item ->
                    item.getAnnotationList() != null && !item.getAnnotationList().isEmpty() && item.getAnnotationList().stream().filter(o -> o.annotationType() == ExternalProperty.class).findFirst().isPresent()
            ).collect(Collectors.toList());
            resolve(name, iocInjectBeanDefinitions);
        }
        logger.info("end postProcess for {}", name);
    }

    private void resolve(String name, List<IocInjectBeanDefinition> iocInjectBeanDefinitions) {
        if (iocInjectBeanDefinitions != null && !iocInjectBeanDefinitions.isEmpty()) {
            for (IocInjectBeanDefinition iocInjectBeanDefinition : iocInjectBeanDefinitions) {
                resolve(name, iocInjectBeanDefinition);
            }
        }
    }

    private void resolve(String name, IocInjectBeanDefinition iocInjectBeanDefinition) {
        try {
            Object object = BeanContainer.getInstance().getContainer().get(name);
            String fieldName = iocInjectBeanDefinition.getName();
            String cFieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            String methodName = SET + cFieldName;
            Annotation annotation = iocInjectBeanDefinition.getAnnotationList().stream().filter(item -> item.annotationType() == ExternalProperty.class).findFirst().get();
            String propertyName = (String) annotation.annotationType().getDeclaredMethod(VALUE).invoke(annotation);
            String propertyValue = PropertiesRegistry.getInstance().getProperties().get(propertyName);
            object.getClass().getMethod(methodName, String.class).invoke(object, propertyValue);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}