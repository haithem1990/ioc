package com.simple.ioc.cycle.post.processors.after;

import com.simple.ioc.container.BeanContainer;
import com.simple.ioc.container.definitions.IocScanBeanDefinition;
import com.simple.ioc.container.definitions.MethodBeanDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

public class PostConstructPostProcess implements PostProcessAfterSet {
    private static final Logger logger = LoggerFactory.getLogger(PostConstructPostProcess.class);

    @Override
    public void postProcess(String name) {
        logger.info("start postProcess for {}",name);
        IocScanBeanDefinition iocScanBeanDefinition = BeanContainer.getInstance().getBeanRegistry().getRegistry().get(name);

        if (iocScanBeanDefinition.getMethodBeanDefinitions() != null && !iocScanBeanDefinition.getMethodBeanDefinitions().isEmpty()) {
            List<MethodBeanDefinition> methodBeanDefinitions = iocScanBeanDefinition.getMethodBeanDefinitions().stream().filter(item ->
                    item.getAnnotationList() != null && !item.getAnnotationList().isEmpty() && item.getAnnotationList().stream().filter(o -> o.annotationType() == PostConstruct.class).findFirst().isPresent()
            ).collect(Collectors.toList());
            resolve(name, methodBeanDefinitions);
        }
        logger.info("end postProcess for {}",name);
    }

    private void resolve(String name, List<MethodBeanDefinition> methodBeanDefinitions) {
        if (methodBeanDefinitions != null && !methodBeanDefinitions.isEmpty()) {
            for (MethodBeanDefinition methodBeanDefinition : methodBeanDefinitions) {
                resolve(name, methodBeanDefinition);
            }
        }
    }

    private void resolve(String name, MethodBeanDefinition methodBeanDefinition) {
        try {
            Object object = BeanContainer.getInstance().getContainer().get(name);
            String methodName = methodBeanDefinition.getName();
            object.getClass().getMethod(methodName).invoke(object);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
