package com.simple.ioc.cycle.post.processors;

public interface PostProcess {
    void postProcess(String name);
}
