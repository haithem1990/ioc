package com.simple.ioc.properties;

import java.util.HashMap;

public interface PropertiesResolver {

    void resolveProperties();
}
