package com.simple.ioc.properties;

import com.simple.ioc.container.PropertiesRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class KeyValuePropertiesResolver implements PropertiesResolver {
    private static final Logger logger = LoggerFactory.getLogger(KeyValuePropertiesResolver.class);
    public static final String PROPERTIES = ".*\\.properties";

    @Override
    public void resolveProperties() {
        logger.info(" start resolveProperties");
        try {
            Properties properties = new Properties();
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("ioc.properties");
            properties.load(inputStream);
            properties.stringPropertyNames().forEach(p -> PropertiesRegistry.getInstance().getProperties().put(p, properties.getProperty(p)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("end resolveProperties");
    }
}
