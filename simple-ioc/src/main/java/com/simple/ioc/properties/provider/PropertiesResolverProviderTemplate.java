package com.simple.ioc.properties.provider;

import com.simple.ioc.properties.KeyValuePropertiesResolver;
import com.simple.ioc.properties.PropertiesResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class PropertiesResolverProviderTemplate {

    private static final Logger logger = LoggerFactory.getLogger(PropertiesResolverProviderTemplate.class);

    public static void resolveProperties() {
        logger.info(" start resolveProperties");
        List<PropertiesResolver> propertiesResolverList = Arrays.asList(new KeyValuePropertiesResolver());
        for (PropertiesResolver propertiesResolver : propertiesResolverList) {
            propertiesResolver.resolveProperties();
        }
        logger.info(" end resolveProperties");
    }
}