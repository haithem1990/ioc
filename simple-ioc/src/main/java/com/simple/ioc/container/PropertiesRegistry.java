package com.simple.ioc.container;

import java.util.LinkedHashMap;
import java.util.Map;

public class PropertiesRegistry {

    private static PropertiesRegistry propertiesRegistry;

    private Map<String, String> properties;

    public static synchronized PropertiesRegistry getInstance() {
        if (propertiesRegistry == null)
            propertiesRegistry = new PropertiesRegistry();
        return propertiesRegistry;
    }

    public PropertiesRegistry() {
        this.properties = new LinkedHashMap<>();
    }

    public static PropertiesRegistry getPropertiesRegistry() {
        return propertiesRegistry;
    }

    public static void setPropertiesRegistry(PropertiesRegistry propertiesRegistry) {
        PropertiesRegistry.propertiesRegistry = propertiesRegistry;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }
}
