package com.simple.ioc.container;

public class BeanContainerUtils {

    private static BeanContainer beanContainer;

    private static BeanContainerUtils beanContainerUtils;

    private BeanContainerUtils() {
        beanContainer = BeanContainer.getInstance();
    }

    public static synchronized BeanContainerUtils getInstance() {
        if (beanContainerUtils == null)
            beanContainerUtils = new BeanContainerUtils();
        return beanContainerUtils;
    }

    public Object getBean(Class<?> generic) {
        return beanContainer.getContainer().get(generic.getName());
    }

    public static BeanContainer getBeanContainer() {
        return beanContainer;
    }

    public static void setBeanContainer(BeanContainer beanContainer) {
        BeanContainerUtils.beanContainer = beanContainer;
    }
}
