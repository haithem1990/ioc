package com.simple.ioc.container;

import java.util.LinkedHashMap;

public class BeanContainer {

    private static BeanContainer beanContainer;

    private LinkedHashMap<String, Object> container;

    private BeanRegistry beanRegistry;

    private PropertiesRegistry propertiesRegistry;

    public static synchronized BeanContainer getInstance() {
        if (beanContainer == null)
            beanContainer = new BeanContainer();
        return beanContainer;
    }

    private BeanContainer() {
        this.beanRegistry = BeanRegistry.getInstance();
        this.propertiesRegistry = PropertiesRegistry.getInstance();
        this.container = new LinkedHashMap<>();
    }

    public static BeanContainer getBeanContainer() {
        return beanContainer;
    }

    public static void setBeanContainer(BeanContainer beanContainer) {
        BeanContainer.beanContainer = beanContainer;
    }

    public LinkedHashMap<String, Object> getContainer() {
        return container;
    }

    public void setContainer(LinkedHashMap<String, Object> container) {
        this.container = container;
    }

    public BeanRegistry getBeanRegistry() {
        return beanRegistry;
    }

    public void setBeanRegistry(BeanRegistry beanRegistry) {
        this.beanRegistry = beanRegistry;
    }

    public PropertiesRegistry getPropertiesRegistry() {
        return propertiesRegistry;
    }

    public void setPropertiesRegistry(PropertiesRegistry propertiesRegistry) {
        this.propertiesRegistry = propertiesRegistry;
    }
}
