package com.simple.ioc.container;

import com.simple.ioc.container.definitions.IocScanBeanDefinition;

import java.util.LinkedHashMap;


public class BeanRegistry {

    private static BeanRegistry beanRegistry;

    private LinkedHashMap<String, IocScanBeanDefinition> registry;

    public static synchronized BeanRegistry getInstance() {
        if (beanRegistry == null) {
            beanRegistry = new BeanRegistry();
        }
        return beanRegistry;
    }

    private BeanRegistry() {
        this.registry = new LinkedHashMap<>();
    }

    public static BeanRegistry getBeanRegistry() {
        return beanRegistry;
    }

    public static void setBeanRegistry(BeanRegistry beanRegistry) {
        BeanRegistry.beanRegistry = beanRegistry;
    }

    public LinkedHashMap<String, IocScanBeanDefinition> getRegistry() {
        return registry;
    }

    public void setRegistry(LinkedHashMap<String, IocScanBeanDefinition> registry) {
        this.registry = registry;
    }
}
