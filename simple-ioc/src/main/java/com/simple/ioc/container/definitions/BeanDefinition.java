package com.simple.ioc.container.definitions;

import java.lang.annotation.Annotation;
import java.util.LinkedList;
import java.util.List;

public class BeanDefinition {

    public String name;

    public List<Annotation> annotationList = new LinkedList<>();


    public BeanDefinition() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Annotation> getAnnotationList() {
        return annotationList;
    }

    public void setAnnotationList(List<Annotation> annotationList) {
        this.annotationList = annotationList;
    }
}
