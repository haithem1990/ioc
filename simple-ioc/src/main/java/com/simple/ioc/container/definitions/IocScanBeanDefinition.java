package com.simple.ioc.container.definitions;

import java.util.LinkedList;
import java.util.List;

public class IocScanBeanDefinition extends BeanDefinition {

    public Class<?> aClass;
    public List<IocInjectBeanDefinition> iocInjectBeanDefinitions = new LinkedList<>();
    public List<MethodBeanDefinition> methodBeanDefinitions = new LinkedList<>();

    public Class<?> getaClass() {
        return aClass;
    }

    public void setaClass(Class<?> aClass) {
        this.aClass = aClass;
    }

    public List<IocInjectBeanDefinition> getIocInjectBeanDefinitions() {
        return iocInjectBeanDefinitions;
    }

    public void setIocInjectBeanDefinitions(List<IocInjectBeanDefinition> iocInjectBeanDefinitions) {
        this.iocInjectBeanDefinitions = iocInjectBeanDefinitions;
    }

    public List<MethodBeanDefinition> getMethodBeanDefinitions() {
        return methodBeanDefinitions;
    }

    public void setMethodBeanDefinitions(List<MethodBeanDefinition> methodBeanDefinitions) {
        this.methodBeanDefinitions = methodBeanDefinitions;
    }
}
